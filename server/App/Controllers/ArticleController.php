<?php

namespace App\Controllers;

use App\Models\ArticleModel;

class ArticleController extends BaseController
{
    public function create()
    {
        $id['id'] = $_SESSION['id'];
        $arr = array_merge($id, $_POST);
        ArticleModel::created($arr);
        //header('user/own_office');
        return self::view('article/articles');
    }

    public function viewArticles()
    {

        return self::view('article/articles');
    }

    public function edit()
    {
        return self::view('article/createArticle');
    }
}
