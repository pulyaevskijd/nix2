<?php

namespace App\Controllers;

use App\Models\UserModel;

class MainController extends BaseController
{
    public function index()
    {
        $user = UserModel::getByEmail($_COOKIE['email']);
        if ($_COOKIE['email'] != '') {
            return $this->view('user/own_office');
        } else {
            return $this->view('user/register');
        }
    }
}
