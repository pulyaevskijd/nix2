<?php

namespace App\Controllers;

use App\Models\UserModel;

class UserController extends BaseController
{

    /**
     * @return void
     */
    public function index(): void
    {
//        $userModel = new ArticleModel();
        $userModel = new UserModel();
        $mUsers = $userModel->where('first_name', '=', 'Daniil');
        echo '<pre>';
        print_r($mUsers);
        echo '</pre>';
    }

    /**
     * @return bool
     */
    public function create(): bool
    {
        UserModel::created($_SESSION);
        $user = UserModel::getByEmail($_SESSION['email']);
        $id = $user[0];
        $_SESSION['id'] = $id['id'];
        //header('user/own_office');
        return self::view('user/own_office');
    }

    /**
     * @return void
     */
    public function getSettings(): void
    {
        $this->view('user/settings');
    }

    /**
     * @return void
     */
    public function getEditUser(): void
    {
        $title = 'Settings';
        $nav = 'main';
        $this->view('user/edit', ['title' => $title, 'nav' => $nav]);
    }


    public function updateUser(): void
    {
        UserModel::updateUser($_POST);
        header('Location: ' . $_ENV['APP_URL'] . '/user/settings');
    }


    /**
     * @return void
     */
    public function delete(): void
    {
//        dd($_SESSION);
        UserModel::del($_SESSION['id']);
        header('Location: ' . $_ENV['APP_URL'] . '/login');
    }


    /**
     * @return bool
     */
    public function own_office(): bool
    {
        $user = UserModel::getByEmail($_SESSION['email']);
        return $this->view('user/own_office', compact('user'));
    }

    /**
     * @return bool
     */
    public function register(): bool
    {
        $text = 'Ця форма для регестрації';
        return $this->view('user/register', ['text' => $text]);
    }

    /**
     * @return bool
     */
    public function check(): bool
    {
        return $this->view('user/check');
    }

    /**
     * @return bool
     */
    public function checkDel():bool
    {
        return $this->view('user/checkDel');
    }

    /**
     * @return bool
     */
    public function auth():bool
    {
        return $this->view('user/auth');
    }

    /**
     * @return bool
     */
    public function login()
    {
        $user = UserModel::getByEmail($_SESSION['email']);
        $id = $user[0];
        $_SESSION['id'] = $id['id'];
        $text = 'Ця форма для вводу авторизіції';
        return $this->view('user/login', ['text' => $text]);
    }
}
