<?php

namespace App\Models;

use PulProduct\Framework\Models\Model;

class ArticleModel extends Model
{
    protected string $table = 'articles';

    protected $created_at = true;
    protected $updated_at = true;

    public static function created($array)
    {

        $user = UserModel::getByEmail($_SESSION['email']);
        $id = $user[0];
//        dd($_SESSION);
        $_SESSION['id'] = $id['id'];
        $articleModel = new self;
//        dd($articleModel);
        $articleModel->insert($array);
    }

    public static function getOne($id)
    {
        $articleModel = new self;
        return $articleModel->whereOne('id', '=', $id);
    }

}
