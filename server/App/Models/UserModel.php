<?php

namespace App\Models;

use PulProduct\Framework\Models\Model;

class UserModel extends Model
{
    protected string $table = 'users';

    protected $created_at = true;
    protected $updated_at = true;

    protected $closeSelection = ['password', 'id'];

    /**
     * @param $array
     * @return void
     */
    public static function created($array)
    {
        $userModel = new self;
        $userModel->insert($array);
    }

    /**
     * @param $id
     * @return array
     */
    public static function getOne($id)
    {
        $userModel = new self;
        return $userModel->whereOne('id', '=', $id);
    }

    /**
     * @param $email
     * @return array
     */
    public static function getByEmail($email)
    {
        $userModel = new self;
        return $userModel->whereOneUs('email', '=', $email);
    }

    /**
     * @param string $id
     * @return void
     */
    public static function del(string $id): void
    {
        $userModel = new UserModel();
        $userModel->delete('id', '=', $id);
        unset($_SESSION);
    }

    /**
     * @param $Data
     */
    public static function updateUser($Data): void
    {
//        dd($Data);
//        dd($_SESSION);
        $userModel = new self;
        $id = $_SESSION['id'];
        if (empty($userModel->update($Data, 'id', $id))) {
            $_COOKIE['first_name'] = $Data['first_name'];
            $_COOKIE['last_name'] = $Data['last_name'];
        }
    }
}
