<?php

use App\Models\UserModel;

$email = filter_var(trim($_POST['email']));
$name = filter_var(trim($_POST['first_name']));
$password = filter_var(trim($_POST['password']));

$user = new UserModel();
$resEm = $user->where('email', '=', $email);

if (mb_strlen($email) < 5 || mb_strlen($email) > 30) {
    echo 'Недопустимая длина Email';
    exit();
} elseif (mb_strlen($name) < 3 || mb_strlen($name) > 30) {
    echo 'Недопустимая длина Імені ';
    exit();
} elseif (mb_strlen($password) < 2 || mb_strlen($password) > 30) {
    echo 'Недопустимая длина Пароля';
    exit();
} elseif (empty($resEm)) {
    $password = md5($password . $_ENV['APP_SOL']);
//    $valId = $resEm[0];
    $_SESSION = $_POST;
    unset($_COOKIE);
    setcookie('email', $email, time() + 3600, "/");
//    setcookie('id', $valId['id'], time() + 3600, "/");

    $_SESSION['password'] = $password;

    header('Location: /user/create');
}else {
    echo 'Користувач вже існує';
    exit();
}
?>
