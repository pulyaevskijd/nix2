<?php $_SESSION['user'] ?? header('Location: '. $_ENV['APP_URL'] . '/');?>
<form class="container form-regist" id="form_regist" name="form_registr" method="POST" action="/user/update">
    <div class="title-regist">
        <h1>Редактировать пользователя</h1>
    </div>
    <div class="mb-3">
        <label class="form-label">Имя:</label>
        <input type="text" maxlength="30" class="form-control" id="first_name" name="first_name" required value="<?=$_SESSION['user']['first_name']?>" placeholder="Введите ваше имя">
    </div>
    <div class="mb-3">
        <label class="form-label">Фамилия:</label>
        <input type="text" maxlength="70" class="form-control" id="last_name" name="last_name" required value="<?=$_SESSION['user']['last_name']?>" placeholder="Введите вашу фамилию">
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Пол:</label>
        <select class="form-select" name="gender">
            <option value="9" selected disabled>Выбирете ваш пол</option>
            <option value="1">Мужчина</option>
            <option value="2">Женщина</option>
            <option value="3">Другое..</option>
        </select>
    </div>
    <div class="mb-3">
        <label for="exampleInputPassword1" class="form-label">Password:</label>
        <input type="password" maxlength="45" class="form-control" id="password" name="password" placeholder="Введите пароль">
    </div>
    <div class="mb-3 error" id="error">
    </div>
    <div class="submit-regist">
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
</form>