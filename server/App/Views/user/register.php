<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
</head>
<body>
<h1>Ця форма для  реєстрації</h1>
<form action='/user/check' method='post'>
    <div class="mb-3">
        <label for="exampleFormControlInput1" class="form-label">First name</label>
        <div class="col-sm-10">
            <input type='text' class="form-control"   name='first_name'>
        </div>
        <label for="exampleFormControlInput1" class="form-label">Last name</label>
        <div class="col-sm-10">
            <input type='text' class="form-control" name='last_name'>
        </div>
        <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
        <div class="col-sm-10">
            <label>
                <input type="password" class="form-control" name="password">
            </label>
        </div>
        <label for="Email" class="form-label">Email address</label>
        <div class="col-sm-10">
            <label>
                <input type='email' class="form-control" name='email'>
            </label>
        </div>
        <div>
            <input class="form-check-input" type="radio" name="gender_id" id="1" value="1" aria-label="..."><label for="radioNoLabel1" class="form-label"> Male</label><br>
            <input class="form-check-input" type="radio" name="gender_id" id="2" value="2" aria-label="..."><label for="radioNoLabel1" class="form-label"> Female</label><br>
        </div>
        <div>
            <label for="a">
                Have account
                <a href="/login">Log in</a>
            </label>

        </div>
        <button type='submit' class="btn btn-primary mb-3">Send</button>
    </div>

</form>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<!--<script>-->
<!--    $('form').on('submit', function (e) {-->
<!--        e.preventDefault()-->
<!--        let data = $(this);-->
<!---->
<!--        console.log(data)-->
<!---->
<!--        $.ajax({-->
<!--            url: $('form').attr('action'),-->
<!--            method: $('form').attr('method'),-->
<!--            dataType: 'json',-->
<!--            data: $(this).serialize(),-->
<!--            success: function (data) {-->
<!--                console.log(data)-->
<!--                // redirect -> старница юзера-->
<!--            }-->
<!--        });-->
<!---->
<!--        console.log(123123)-->
<!--    })-->
</script>
</body>
</html>



