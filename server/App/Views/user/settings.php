<?php $_SESSION['user'] ?? header('Location: '. $_ENV['APP_URL'] . '/');
?>
<form class="container form-regist" id="form_regist" name="form_registr" method="POST" action="/user/update">
    <div class="title-regist">
        <h1>Настройки пользователя</h1>
    </div>
    <div class="mb-3">
        <label class="form-label"><b>Имя:</b> <?=$_SESSION['user']['first_name']?></label>
    </div>
    <div class="mb-3">
        <label class="form-label"><b>Фамилия:</b> <?=$_SESSION['user']['last_name']?></label>
    </div>
    <div class="mb-3">
        <label class="form-label"><b>Почта:</b> <?=$_SESSION['user']['email']?></label>
    </div>
    <div class="mb-3 error" id="error">
    </div>
    <div class="submit-regist">
        <a class="btn btn-primary" href="/user/edit">Редактировать</a>
        <a class="btn btn-danger" href="/user/delete">Удалить пользователя</a>
    </div>
</form>
