<?php

namespace PulProduct\Framework\Controllers;

use PulProduct\Framework\Views\View;

/**
 * Class Controller.
 *
 * @author Alexander Zabara <zabara.industry@gmail.com>
 */
abstract class Controller
{
    /**
     * @var array
     */
    public array $route = [];

    /**
     * @param $route
     */
    public function __construct($route)
    {
        $this->route = $route;
    }

    /**
     * @param string $view
     * @param array $data
     * @return bool
     */
    public function view(string $view, array $data = []): bool
    {
        $viewObject = new View($this->route, $view);
        $viewObject->render($data);
        return true;
    }
}