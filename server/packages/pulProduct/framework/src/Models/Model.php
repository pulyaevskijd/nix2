<?php

namespace PulProduct\Framework\Models;

use PulProduct\Framework\DB;

abstract class Model
{
    /**
     * @var DB
     */
    protected $pdo;
    protected string $table;

    protected $created_at = false;
    protected $updated_at = false;

    protected $closeSelection = false;

    public function __construct()
    {
        $this->pdo = DB::instance();
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        $sql = "SELECT * FROM $this->table";
        return $this->pdo->query($sql);
    }

    /**
     * @param $search
     * @param $id
     * @return array
     */
    public function where($search, $operator, $id): array
    {
        $sql = "SELECT * FROM $this->table WHERE $search $operator '$id'";
//        $strCloserSelection = implode(',', $this ->closeSelection);
        return $this->pdo->query($sql);
    }

    /**
     * @param $search
     * @param $operator
     * @param $id
     * @return array
     */
    public function whereOneUs($search, $operator, $id)
    {
        $sql = "SELECT * FROM users WHERE $search $operator '$id' LIMIT 1";
//        $strCloserSelection = implode(',', $this ->closeSelection);
        return $this->pdo->query($sql);
    }

    /**
     * @param $search
     * @param $operator
     * @param $id
     * @return array
     */
    public function whereOne($search, $operator, $id)
    {
        $sql = "SELECT * FROM $this->table WHERE $search $operator '$id' LIMIT 1";
//        $strCloserSelection = implode(',', $this ->closeSelection);
        return $this->pdo->query($sql);
    }

    /**
     * @param $whereColumn
     * @param $operator
     * @param $data
     * @return array
     */
    public function delete($whereColumn, $operator, $data): array
    {
        $sql = "DELETE FROM $this->table WHERE $whereColumn $operator '$data'";
        return $this->pdo->query($sql);
    }


    /**
     * @param $array
     * @return array
     */
    public function insert($array): array
    {
        $array = array_map(function ($val) {
            return htmlspecialchars($val);
        }, $array);

        if ($this->created_at && $this->updated_at) {
            $arr = [
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s'),
            ];
            $array = array_merge($array, $arr);
        }

        $keysString = implode(',', array_keys($array));
//        dd($keysString);
        $valuesString = implode('\',\'', array_values($array));

        $sql = "INSERT INTO $this->table ($keysString) VALUES ('$valuesString')";

//        dd($sql);
        return $this->pdo->query($sql);
    }

    /**
     * @param $array
     * @param $column
     * @param $data
     * @return array
     */
    public function update($array, $column, $data): array
    {

        $array = array_map(function ($val) {
            return htmlspecialchars($val);
        }, $array);

        if ($this->created_at && $this->updated_at) {
            $arr = [
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $array = array_merge($array, $arr);
        }

        $set = '';
        $x = 1;
        foreach ($array as $key => $value) {
            $set .= "$key = '$value'";
            if ($x < count($array)) {
                $set .= ', ';
            }
            $x++;
        }

        $sql = "UPDATE $this->table SET $set WHERE $column = $data;";
        return $this->pdo->query($sql);
    }
}
