<?php

namespace PulProduct\Framework;

class Route
{
    /**
     * @var array
     */
    protected static array $routes = [];

    /**
     * @var array
     */
    protected static array $route = []; // [UserController::class, 'index']

    /**
     * @param string $url
     * @param array $route
     * @return void
     */
    public static function add(string $url, array $route = []): void
    {
        self::$routes[$url] = $route;
    }

    /**
     * @param string $url
     * @return void
     */
    public static function dispatch(string $url): void
    {

        if (self::matchRoute($url)) {
            $controller = self::$route[0];
            if (class_exists($controller)) {
                $cObj = new $controller(self::$route);
                $action = self::$route[1];

                if (method_exists($cObj, $action)) {
                    $cObj->$action();
                } else {
                    echo 'Метод ' . $action . ' не существует';
                }
            } else {
                echo 'Controller ' . $controller . ' не найден';
            }
        } else {
            http_response_code(404);
            include '404.html';
        }
    }

    /**
     * @param string $url
     * @return bool
     */
    public static function matchRoute(string $url): bool
    {
        foreach (self::$routes as $pattern => $route) {
            if ($pattern == $url) {
                self::$route = $route;
                return true;
            }
        }
        return false;
    }
}
