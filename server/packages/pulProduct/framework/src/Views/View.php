<?php

namespace PulProduct\Framework\Views;

/**
 * Class View.
 * @author Alexander Zabara <zabara.industry@gmail.com>
 */
class View
{
    /**
     * @var array
     */
    public array $route = [];

    /**
     * @var string
     */
    public string $view;

    /**
     * @param array $route
     * @param string $view
     */
    public function __construct(array $route, string $view = '')
    {
        $this->route = $route;
        $this->view = $view;
    }

    /**
     * @param $data
     * @return void
     */
    public function render($data): void
    {
        if (is_array($data)){
            extract($data);
        }

        $fileView = ROOT . '/App/Views/' . $this->view . '.php';

        if (is_file($fileView)) {
            require $fileView;
        } else {
            echo '<h1> Файл не знайден! ' . $fileView . '</h1>';
        }
    }
}