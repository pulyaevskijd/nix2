<?php
/**
 * @param $array
 * @return void
 */
function dump($array): void
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

/**
 * @param $array
 * @return void
 */
function dd($array):void
{
    dump($array);
    die();
}
