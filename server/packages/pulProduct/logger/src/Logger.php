<?php

namespace PulProduct\Logger;

class Logger
{
    public static function log($context)
    {
        self::method(require 'config/log.php')->writeLog(__FUNCTION__, $context);
    }

    public static function error($context)
    {
        self::method(require 'config/log.php')->writeLog(__FUNCTION__, $context);
    }

    public static function method($class)
    {
        $listenerClass = 'ZabaraIndustry\\Logger\\Methods\\' . ucfirst($class) . 'ClassFactory';

        if (class_exists($listenerClass)) {
            return $listenerClass::getInstance();
        } else {
            exit('This class does not exist!' . ucfirst($class) . 'ClassFactory');
        }
    }
}