<?php

namespace PulProduct\Logger\Methods;

interface MethodInterfaceFactory
{
    public function writeLog($level, $message);
}