<?php
require_once '../vendor/autoload.php';
require_once '../packages/pulProduct/framework/src/function.php';

session_start();

define('URL', trim($_SERVER['REQUEST_URI'], ''));
define('ROOT', dirname(__DIR__));

use Symfony\Component\Dotenv\Dotenv;
use PulProduct\Framework\Route;

$dotenv = new Dotenv();
$dotenv->load(ROOT . '/.env');

require_once ROOT . '/routes/web.php';

Route::dispatch(URL);
