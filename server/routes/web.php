<?php

use App\Controllers\ArticleController;
use App\Controllers\MainController;
use App\Controllers\UserController;
use PulProduct\Framework\Route;

Route::add('/', [MainController::class, 'index']);

Route::add('/login', [UserController::class, 'login']);
Route::add('/register', [UserController::class, 'register']);
Route::add('/auth', [UserController::class, 'auth']);

Route::add('/user', [UserController::class, 'index']);

Route::add('/user/create', [UserController::class, 'create']);
Route::add('/user/update', [UserController::class, 'updateUser']);
Route::add('/user/settings', [UserController::class, 'getSettings']);
Route::add('/user/edit', [UserController::class, 'getEditUser']);

Route::add('/user/checkDel', [UserController::class, 'checkDel']);
Route::add('/user/delete', [UserController::class, 'delete']);

Route::add('/user/own_office', [UserController::class, 'own_office']);
Route::add('/user/check', [UserController::class, 'check']);

Route::add('/article/createArticle', [ArticleController::class, 'edit']);
Route::add('/article/create', [ArticleController::class, 'create']);



/**
 * 1. url со Всеми статьями автора
 *
 * 2.
 */
